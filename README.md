# Livro de Robótica Móvel para Engenharia

[![License: CC BY-NC-ND 4.0](https://licensebuttons.net/l/by-nc-nd/4.0/80x15.png)](https://creativecommons.org/licenses/by-nc-nd/4.0/)

## TODO List
* [x] **Capítulo 1** - _Introdução a Robótica_.
* [x] **Capítulo 2** - _Locomoção e Percepção_.
* [x] **Capítulo 3** - _Simulação de Robôs Móveis_.
* [ ] **Capítulo 4** - _Controle Clássico para Sistemas Robóticos_.
* [ ] **Capítulo 5** - _Controle Moderno para Sistemas Robóticos_.
* [ ] **Capítulo 6** - _Localização e mapeamento simultâneos (SLAM)_.

```mermaid
gantt
dateFormat  YYYY-MM-DD
title Cronograma do Livro
excludes weekdays 2014-01-10

section A section
Introdução              :           des1, 2021-10-01,2021-11-30
Locomoção e Percepção   :           des2, 2021-11-01,2021-12-30
Simulação               :           des3, 2021-11-01,2021-12-30
Simulação               :           des4, 2022-02-01,2022-03-30
Controle Clássico       :active,    des5, 2022-03-01,2022-04-30
Controle Moderno        :active,    des6, 2022-04-01,2022-05-30
SLAM                    :           des7, 2022-05-01,2022-07-30
```

## Bibliografia
- Roland Siegwart, Illah Reza Nourbakhsh, Davide Scaramuzza. **Introduction to Autonomous Mobile Robots** (Intelligent Robotics and Autonomous Agents series), 2nd ed. The MIT Press, 2011.
- Sebastian Thrun, Wolfram Burgard, Dieter Fox. **Probabilistic Robotics** (Intelligent Robotics and Autonomous Agents series). The MIT Press, 2005.
- Ulrich Nehmzow. **Mobile Robotics: A Practical Introduction**, 2nd ed. Springer, 2003.

## Bibliografia Complementar

- CRAIG, John J. **Introduction to Robotics: Mechanics and Control**. 1989.
- JOSEPH, Lentin. **ROS Robotics projects**. Packt Publishing Ltd, 2017.
- KLANCAR, Gregor et al. **Wheeled mobile robotics: from fundamentals towards autonomous systems**. Butterworth-Heinemann, 2017.
- LYNCH, Kevin M.; PARK, Frank C. **Modern robotics**. Cambridge University Press, 2017.
- MARTINEZ, Aaron; FERNÁNDEZ, Enrique. **Learning ROS for robotics programming**. Packt Publishing Ltd, 2013.
- ROMERO, Roseli AF et al. **Robótica móvel**. Sao Paulo: LTC, 2014.
- SICILIANO, Bruno; KHATIB, Oussama (Ed.). **Springer handbook of robotics**. springer, 2016.
- SIEGWART, Roland; NOURBAKHSH, Illah Reza; SCARAMUZZA, Davide. **Introduction to autonomous mobile robots**. MIT press, 2011.

