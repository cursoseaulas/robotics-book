#####################################################
# LaTeX Makefile
#
# Latex Compile and Clean
#
# Author: Jeferson
#####################################################

TARGET = main
# ARGS = -synctex=1 -interaction=nonstopmode --shell-escape 
LOG = /tmp/build_output.log
build:
	-@pdflatex $(ARGS) $(TARGET).tex > $(LOG)
	-@echo "build  (1x)."
	-@bibtex           $(TARGET).aux > $(LOG)
	-@echo "bibtex (1x)."
	-@pdflatex $(ARGS) $(TARGET).tex > $(LOG)
	-@echo "build  (2x)."
	-@pdflatex $(ARGS) $(TARGET).tex > $(LOG)
	-@echo "build  (3x)."
	-@echo "build project done."
	-@tail -n 3 $(LOG)


build_once:
	-pdflatex $(ARGS) $(TARGET).tex;
	-@echo "build project done."

clean:
	-$(RM) *.acn *.acr *.alg *.aux *.bbl \
			*.blg *.dvi *.fdb_latexmk *.glg *.glo \
			*.gls *.idx *.ilg *.ind *.ist \
			*.lof *.log *.lot *.maf *.mtc \
			*.mtc0 *.nav *.nlo *.out *.pdfsync \
			*.pyg *.snm *.synctex.gz *.thm *.toc \
			*.vrb *.xdy *.tdo *.spl *.auxlock 
	-@echo "clean project done."
	-$(RM) ./epsfigs/*.spl ./epsfigs/*.dep ./epsfigs/*.dpth \
		./epsfigs/*.log ./epsfigs/*.md5 ./epsfigs/*.pdf \
		./figures/*-eps-converted-to.pdf
	-@echo "clean output figures."


.PHONY:build build_once clean
